<?php if (!isset($_GET['size'])) $_GET['size'] = 1;
$size = array("width" => 500, "height" => 500);

if ($size['width'] && $size['height']){

        $img = @imagecreatetruecolor($size['width'], $size['height']);
        $text_color = imagecolorallocate($img, 14, 233, 91);
        imagestring($img, 8, 5, 5, "z=" . $_GET['z'], $text_color);
        imagestring($img, 8, 5, 20, "x=" . $_GET['x'] . " y=" . $_GET['y'], $text_color);
        header('Content-type: image/png');
        echo imagepng($img);
        imagedestroy($img);

} else {

        http_response_code(404);

}